Copyright 2013 Yuriy Shevchuk

This work contains URI.js by Rodney Rehm (http://rodneyrehm.de/en/).
URI.js is published under the MIT license (http://opensource.org/licenses/mit-license) 
and GPL v3 (http://opensource.org/licenses/GPL-3.0).
