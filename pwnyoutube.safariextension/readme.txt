pwnyoutube Safari extension

This is a Safari extension which primary goal is to redirect all requests which point
to youtube.com to another service deturl.com (or pwnyoutube.com). Author is not affiliated
with deturl.com.

This was born because I grew tired to receive large links to youtube videos thru chat and
then (since I don't like to be logged in to google's services) login every time if the 
video is for those who are over 18 and also see all suggestions and other stuff of youtube.
I googled and deturl.com was the first easy service to avoid all this mess. Maybe there 
are others. So I've simply wrote this small extension which basically rewrites youtube.com
and youtu.be links to deturl.com links.

Cheers.

Your author.
