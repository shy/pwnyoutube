function handleOpen(e) {
	if (e.target instanceof SafariBrowserTab) {
		e.target.addEventListener('beforeNavigate', changeurl, false);
	}
}

function changeurl(event) {
	var searchPwnYoutube = "pwnyoutu";
	var searchDeturl = "deturl";
	
	var indexDeturl = event.url.indexOf(searchDeturl);
	var indexPwnYoutube = event.url.indexOf(searchPwnYoutube);
	
	var uri = new URI(event.url);
	
	var foundDeturl = (uri.hostname() === "deturl.com");
	var foundPwnYoutube = (uri.hostname() === "pwnyoutube.com");
		
	if (!foundDeturl && !foundPwnYoutube) {
		
		var foundYoutuBe = (uri.hostname() === "youtu.be");
		var foundYoutubeCom = (uri.hostname() === "youtube.com") || (uri.hostname() === "www.youtube.com");

		var needToReplace = false;
		var substituteString = event.url;

		if (foundYoutuBe) {
			var path = uri.path();
			var pathElements = path.split("/");
			// in links like youtu.be/somePath we expect 2 elements "" and "somePath"
			if (pathElements == 2) {
				substituteString = "http://deturl.com/play.asp?v=" + pathElements[1];
				needToReplace = true;
			}
		}
		else if (foundYoutubeCom) {
			// from links like youtube.com/watch?v=somePath&feature=.... we take out only somePath
			// and put it directly into deturl
			var query = uri.query();
			var keyValuesArray = query.split("&");
			for (var i = 0; i < keyValuesArray.length; i++) {
				var parameter = keyValuesArray[i].split("=")[0];
				if (parameter == "v") {
					substituteString = "http://deturl.com/play.asp?v=" + keyValuesArray[i].split("=")[1];
					needToReplace = true;
					break;
				}
			}
		}
		/* check if we need/can replace and if we really replacing in correct tab since safari sends
		this event to old tab when you want to open new*/
		if (needToReplace && (event.url == event.target.url || event.target.url == "")) {
			event.target.url = substituteString;
		}
	}
}

safari.application.addEventListener("open", handleOpen, true);
 				